# Load pre-reqs
source("init.R")


#*** Data set up ***      
# Souvenir data used in https://a-little-book-of-r-for-time-series.readthedocs.io/en/latest/src/timeseries.html
souvenir <- scan("http://robjhyndman.com/tsdldata/data/fancy.dat")
# local data
# scrabble
scrabble<-readxl::read_xlsx("Scrabble.xlsx")



library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("Time Series"),

    # Sidebar with a slider input 
    # For 1) selecting data 2) creating TS variable 3) 
    # creating TS variable (x - var and Y)
    # uniting 
    # graphing 
    # ## 
    # 
    #
    sidebarLayout(
        sidebarPanel(
            # dataset selection 
            selectInput(inputId = "dataset",
                        label = "Choose a dataset:",
                        choices = c(
                                    "Scrabble", 
                                    "Souvenier")), 
            actionButton("button1", "Confirm Data Selection"),

             # GG Plot
        selectInput(inputId = "IV",
                    label = "Choose Independent Variable:",
                    choices = "Nothing Selected"), 
    
        actionButton("button1", "Confirm Data Selection")
    ), # end sidebar      
   # Diplay content      
                tabsetPanel(position = "above",
                    tabPanel("Data",tableOutput("contents")))
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

    dat1<- eventReactive(input$button1,{
        switch(input$dataset,
               "Scrabble"= scrabble,
               "Souvenier" = souvenir)
        
    })
    
    
    # Sidebar nav
    observe({
        # requires file 1
        #    req(input$file1)
        dsnames <- names(dat1())
        cb_options <- list()
        cb_options[dsnames] <- dsnames
        updateSelectInput(session, "IV",
                          label = NULL,
                          choices = cb_options,
                          selected = "")
    })
    
    
    # display data
    output$contents <- renderTable({
        dat1()
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
