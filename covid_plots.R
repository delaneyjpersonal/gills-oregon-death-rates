

## Cumulative cases per RUCC 

Rucc_cumulative<-ggplot(Merge_oregon_filter_mutate,
       aes(x = date_date, y = CUM_RUCC_cases)) +
  geom_line(aes(colour=`RUCC 2013`), size=1) +
  theme(axis.text.x = element_text(angle = 90)) + 
  scale_x_date(date_breaks = "1 week", date_labels =  "%b %d") +
  scale_color_manual(values = group.colors,
                     labels = 
                       rucc_examples_county ) +
  labs(title = "Cumulative Oregon Covid-19 Cases by County") + ylab("Total Cases") + xlab("Week")


unique(Rucc_cumulative$data[[1]]["fill"])

# change colors so they make more sense 
group.colors <- c("#000000", #Black 
                  "#00FF00",  # Bright green
                  "#000066",  # dark blue 
                  "#CC0000",  # red
                  "#CCFF00", # Yellow
                  "#66FF99", # torquise
                  "#00FFCC",   # light blue
                  "#660000",    # brown
                  "#FF9966")   # peach

rucc_lab<-c("1: Metro areas > 1 million population", #Clackamas County
            "2: Metro areas > 250,000", #Lane County
            "3: Metro areas < 250,000", #Benton County, 
            "4: Non-Metro metro adjacent > 20,00 pop",
            "5: Non-Metro, not-metro adjacent \n > 20,000 pop", 
            "6: Non-Metro, metro adjacent \n 2500 < pop < 20000 ", #Nonmetro - Urban population of 2,500 to 19,999, adjacent to a metro area 
            "7: Non-Metro not-metro adjacent \n 2500 < pop < 20000", 
            "8: Non-Metro, metro adjacent \n  < 2500", #Nonmetro - Completely rural or less than 2,500 urban population, adjacent to a metro area                                                                                                               
            "9: Non-Metro, not-metro adjacent \n < 2500")

rucc_examples_county <- c("1: Col.,,Yam.,Clack., Wash., Mult.", #Clackamas County
                          "2: Polk, Marion,", #Lane County
                          "3: Benton, Deschutes, Linn", #Benton County, 
                          "4: Clatsop, Umatilla ",
                          "5: Lincoln, Coos, Klamath ", 
                          "6: Tillamook,Wasco,Hood,Malheur ", #Nonmetro - Urban population of 2,500 to 19,999, adjacent to a metro area 
                          "7: Baker, Curry, Harney ", 
                          "8: ", #Nonmetro - Completely rural or less than 2,500 urban population, adjacent to a metro area                                                                                                               
                          "9: Wheeler, Wallowa")


### Using cumulative cases ()




# Plot overall cumulative 
total_cases_cumulative<-ggplot(Merge_oregon_filter_mutate,
       aes(x = date_date, y = overall_cumulative_cases)) + 
  geom_line() +
  theme(axis.text.x = element_text(angle = 90)) +
  scale_x_date(date_breaks = "1 week", date_labels =  "%b %d") +
  labs(title = "Cumulative Covid-19 Cases in Oregon") + ylab("Total Cases") + xlab("Week")


# County plot cumulative
county_cases_cumulative <- ggplot(Merge_oregon_filter_mutate,
       aes(x = date_date, y = cumulative_county_cases, group= county)) + 
  geom_line(aes(color=county)) +
  theme(axis.text.x = element_text(angle = 90)) +
  scale_x_date(date_breaks = "1 week", date_labels =  "%b %d") +
  labs(title = "Cumulative Oregon Covid-19 Cases by County") + ylab("Total Cases") + xlab("Week")

# daily cases

daily_cases_new<-ggplot(Merge_oregon_filter_mutate,
                               aes(x = date_date, y = daily_county_cases, group= county)) + 
  geom_line(aes(color=county)) +
  theme(axis.text.x = element_text(angle = 90)) +
  scale_x_date(date_breaks = "1 week", date_labels =  "%b %d") +
  labs(title = "Daily Covid-19 Cases in Oregon by County") + ylab("New Cases") + xlab("Week") +
  labs(caption = "Negative rate implies more death/recoveries than cases")


